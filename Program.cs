﻿using System;
using caalhp.IcePluginAdapters;

namespace EventAnalyserService
{
	/// <summary>
	/// Look at all incomming event, compare them with other events
	/// and determine whether they are false positive or not.
	/// </summary>
	class Program
	{
		static void Main(string[] args)
		{
			//we assume the host is running on localhost:
			const string endpoint = "localhost";

			try
			{
				Console.WriteLine("EventAnalyserService started");
				var service = new EventAnalyser(new InterpreterContextEvent(new EventDatabase()));
				var adapter = new ServiceAdapter(endpoint, service);

				Console.WriteLine("EventAnalyserService running");
		
				Console.ReadKey();
				Console.WriteLine("press <enter> to exit...");
				Console.ReadLine();
				service.ShutDown();
			}
			catch (Exception ex)
			{
				//Connection to host probably failed
				Console.WriteLine(ex.Message);
				Console.ReadLine();
			}
		}
	}
}
