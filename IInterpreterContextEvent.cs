﻿using System;
using System.Collections.Generic;
using EventTypes;

namespace EventAnalyserService
{
	public interface IInterpreterContextEvent
	{
		List<BaseEvent> Events { get; }
		void Add(BaseEvent evt);
		void AnalyseEvents();
		bool ShouldTakeAction(BaseEvent newEvent);
		List<BaseEvent> ReadPreviousEventDB(Type typeToLookfor, int timeToLookBack);
		int ProbabilityAffection(BaseEvent newEvent, IEnumerable<BaseEvent> eventsFound);
		Dictionary<Type, int> EventsAffectingIt(BaseEvent newEvent);
		void InsertEvent(BaseEvent baseEvent);
	}
}