﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EventTypes;

namespace EventAnalyserService
{
	public interface IEventDatabase
	{
		void EmptyNewEventsDatabase();
		void EmptyLatestsEventsDatabase();
		void EmptyPublishedEventsDatabase();
		void InsertEventInNewEventsDatabase(BaseEvent inputBaseEvent);
		void InsertEventInLatestEventsDatabase(BaseEvent inputBaseEvent);
		BaseEvent GetNewEventDocument();
		BaseEvent PopNewEventDocument();
		List<BaseEvent> GetNewEventSubTypeDocument(Type filterType);
		List<BaseEvent> FindNewTypeEvents(Type typeFilter);
		Task<List<BaseEvent>> GetAllLatestDocuments();
		Task<List<BaseEvent>> GetAllLatestSubTypeDocuments(Type lookupType);
		int GetNewEventsDocumentCount();
		void PublishConclusion(BaseEvent newEvent);
	}
}