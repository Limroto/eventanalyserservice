﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EventTypes;
using EventTypes.TestEvents;
using NSubstitute;
using NUnit.Framework;

namespace EventAnalyserService.Tests
{
	[TestFixture]
	public class InterpreterContextEventInsertTest
	{
		private IInterpreterContextEvent _interpreter;
		private IEventDatabase _eventDatabase;

		[SetUp]
		public void Setup()
		{
			RegisterHelper.CheckBaseLvl();
			_eventDatabase = Substitute.For<IEventDatabase>();
			_interpreter = new InterpreterContextEvent(_eventDatabase);
		}

		[TearDown]
		public void TearDown()
		{
			RegisterHelper.Instance.RemoveAffections();
			_interpreter = null;
			_eventDatabase = null;
		}

		[Test]
		public void ValidateEventIsShimmer_Succes()
		{
			var list = new List<int>() { 1, 2, 3 };
			var probability = 85;
			var shimmer = new Shimmer { Data = list, Condition = probability };

			_interpreter.Add(shimmer);

			Assert.That(_interpreter.Events[0], Is.TypeOf<Shimmer>());
		}

		[Test]
		public void ValidateEventIsShimmer_Fail()
		{
			var list = new List<int>() { 1, 2, 3 };
			var probability = 85;
			var dummy = new DummyFall {Data = list, Condition = probability};

			_interpreter.Add(dummy);

			Assert.That(_interpreter.Events[0], Is.Not.TypeOf<Shimmer>());
		}

		[Test]
		public void ExtractShimmerFallEventData_Succes()
		{
			var list = new List<int>() { 1, 2, 3 };
			var probability = 85;
			var shimmer = new Shimmer { Data = list, Condition = probability };

			_interpreter.Add(shimmer);
			var shimmerEvent = _interpreter.Events[0];

			Assert.That(shimmerEvent.Condition, Is.EqualTo(probability));
			Assert.That(shimmerEvent.Data, Is.EqualTo(list));
		}

		[Test]
		public void ShouldTakeAction_BasicElement_ShouldNot()
		{
			var list = new List<int>() { 1, 2, 3 };
			var probability = true;
			var dummy = new DummyBed { Data = list, Condition = probability };

			_eventDatabase.GetNewEventDocument().Returns(dummy);
			var extracted = _eventDatabase.GetNewEventDocument();

			var res = _interpreter.ShouldTakeAction(extracted);

			Assert.That(res, Is.EqualTo(false));
		}

		[Test]
		public void ReadPreviousEventDB_FindTooOldEvent_ReturnNothing()
		{
			var list = new List<int>() { 1, 2, 3 };
			var probability = true;
			var dummy = new DummyBed { Data = list, Condition = probability };

			dummy.TimeStamp = DateTime.Now.AddSeconds(-12).ToString();

			var listToReturn = new List<BaseEvent>() { dummy };
			_eventDatabase.GetAllLatestSubTypeDocuments(Arg.Any<Type>()).Returns(Task.FromResult(listToReturn));

			var possibleEvents = _interpreter.ReadPreviousEventDB(typeof(BedEvent), 10);
			Assert.That(possibleEvents.Count, Is.EqualTo(0));
		}

		[Test]
		public void ReadPreviousEventDB_FindNewEvent_ReturnOneEvent()
		{
			var list = new List<int>() { 1, 2, 3 };
			var probability = true;
			var likelyhood = 85;
			var dummyBed = new DummyBed { Data = list, Condition = probability };
			var dummyFall = new DummyFall { Data = list, Condition = likelyhood };

			dummyBed.TimeStamp = DateTime.Now.AddSeconds(-2).ToString();

			var listToReturn = new List<BaseEvent>() { dummyBed };
			_eventDatabase.GetAllLatestSubTypeDocuments(Arg.Any<Type>()).Returns(Task.FromResult(listToReturn));

			var possibleEvents = _interpreter.ReadPreviousEventDB(typeof(BedEvent), 10);
			Assert.That(possibleEvents.Count, Is.EqualTo(1));
		}

		[Test]
		public void ShouldTakeAction_AdvanceElement_Should()
		{
			var list = new List<int>() { 1, 2, 3 };
			var probability = 85;
			var dummyFall = new DummyFall { Data = list, Condition = probability };

			_eventDatabase.GetNewEventDocument().Returns(dummyFall);
			var extracted = _eventDatabase.GetNewEventDocument();

			var res = _interpreter.ShouldTakeAction(extracted);

			Assert.That(res, Is.EqualTo(true));
		}

		[Test]
		public void EventsAffectingIt_NotAffectedEvent_EmptyListReturned()
		{
			RegisterHelper.Instance.AddAffection(typeof(DummyFall), typeof(BedEvent), 10);


			var list = new List<int>() { 1, 2, 3 };
			var probability = true;
			var dummyBed = new DummyBed { Data = list, Condition = probability };

			var affected = _interpreter.EventsAffectingIt(dummyBed);
			Assert.That(affected.Count, Is.EqualTo(0));
		}

		[Test]
		public void EventsAffectingIt_AffectedEvent_ListReturned()
		{
			RegisterHelper.Instance.AddAffection(typeof(DummyFall), typeof(BedEvent), 10);

			var list = new List<int>() { 1, 2, 3 };
			var likelyhood = 85;
			var dummyFall = new DummyFall { Data = list, Condition = likelyhood };

			var affected = _interpreter.EventsAffectingIt(dummyFall);
			Assert.That(affected.Count, Is.EqualTo(1));
		}

		[Test]
		public void EventsAffectingIt_OldEvent_NoListReturned()
		{
			RegisterHelper.Instance.AddAffection(typeof(DummyFall), typeof(BedEvent), 10);

			var list = new List<int>() { 1, 2, 3 };
			var likelyhood = 90;
			var dummyFall = new DummyFall { Data = list, Condition = likelyhood};

			var affected = _interpreter.EventsAffectingIt(dummyFall);
			Assert.That(affected.Count, Is.EqualTo(1));
		}

		[Test]
		public void ProbabilityAffection_FallEventWithBedEvent_NotLikely()
		{
			var list = new List<int>() { 1, 2, 3 };
			var likelyhood = 80;
			var dummyFall = new DummyFall { Data = list, Condition = likelyhood };

			var eventsFound = new List<BaseEvent>() { new DummyBed{Data = new List<int>(), Condition = true} };

			likelyhood = _interpreter.ProbabilityAffection(dummyFall, eventsFound);
			Assert.That(likelyhood, Is.EqualTo(40));
		}

		[Test]
		public void AnalyseEvents_NoNewEvents_ReturnNothing()
		{
			_eventDatabase.GetNewEventDocument().Returns(a => null);
			var called = false;

			_eventDatabase
				.When(c => c.PublishConclusion(Arg.Any<BaseEvent>()))
				.Do(c => called = true);

			_interpreter.AnalyseEvents();
			Assert.That(called, Is.EqualTo(false));
		}

		//[Test]
		//public void AnalyseEvents_NewEvent_EventPublished()
		//{
		//	_eventDatabase.PopNewEventDocument().Returns(new DummyBed { Data = new List<int>(), Condition = true });
		//	var called = false;

		//	_eventDatabase
		//		.When(c => c.PublishConclusion(Arg.Any<BaseEvent>()))
		//		.Do(c => called = true);

		//	_interpreter.AnalyseEvents();
		//	Assert.That(called, Is.EqualTo(true));
		//}

		[Test]
		public void AnalyseEvents_NewEventReal_EventPublished()
		{
			_eventDatabase = new EventDatabase();
			_interpreter = new InterpreterContextEvent(_eventDatabase);
			
			_eventDatabase.EmptyNewEventsDatabase();
			_eventDatabase.EmptyLatestsEventsDatabase();
			_eventDatabase.EmptyPublishedEventsDatabase();

			var dummy = new DummyBed { Data = new List<int>{1,2,3}, Condition = true };
			RegisterHelper.RegisterNewClass(dummy);
			_eventDatabase.InsertEventInNewEventsDatabase(dummy);
			_interpreter.AnalyseEvents();

			var docs = _eventDatabase.GetAllLatestDocuments().Result;
			Assert.That(docs.Count, Is.EqualTo(1));
		}

		[Test]
		public void AnalyseEvents_NewEventSameTypeAllreadyStored_OldEventOverriden()
		{
			_eventDatabase = new EventDatabase();
			_interpreter = new InterpreterContextEvent(_eventDatabase);

			var dummy1 = new DummyBed { Data = new List<int>(), Condition = true };
			var dummy2 = new DummyBed { Data = new List<int>(), Condition = false };
			RegisterHelper.RegisterNewClass(dummy1);

			_eventDatabase.InsertEventInNewEventsDatabase(dummy1);
			_interpreter.AnalyseEvents();

			_eventDatabase.InsertEventInNewEventsDatabase(dummy2);
			_interpreter.AnalyseEvents();

			var docs = _eventDatabase.GetAllLatestDocuments().Result;
			Assert.That(docs.Count, Is.EqualTo(1));
			Assert.That(docs[0].Condition, Is.EqualTo(0));
		}
	}
}