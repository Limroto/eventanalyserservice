﻿using System.Collections.Generic;
using System.Threading;
using EventTypes;
using EventTypes.TestEvents;
using NUnit.Framework;

namespace EventAnalyserService.Tests
{
	[TestFixture]
	public class InsertEventIntoDatabaseTest
	{
		private EventDatabase _eventDatabase;

		[SetUp]
		public void Setup()
		{
			_eventDatabase = new EventDatabase();
			RegisterHelper.RegisterNewClass(new DummyBed());
			RegisterHelper.RegisterNewClass(new DummyFall());
			_eventDatabase.EmptyNewEventsDatabase();
			_eventDatabase.EmptyLatestsEventsDatabase();
			_eventDatabase.EmptyPublishedEventsDatabase();
		}

		[TearDown]
		public void TearDown()
		{
			_eventDatabase = null;
		}

		[Test]
		public void GetNewEventDocument_NoDocuments_ReturnNull()
		{
			var data = _eventDatabase.GetNewEventDocument();
			Assert.That(data, Is.EqualTo(null));
		}

		[Test]
		public void GetNewEventDocument_DummyBedDocuments_RetunsDummyBed()
		{
			var dummy = new DummyBed { Condition = true };
			_eventDatabase.InsertEventInNewEventsDatabase(dummy);

			var doc = _eventDatabase.GetNewEventDocument();
			Assert.That(doc, Is.TypeOf<DummyBed>());
		}

		[Test]
		public void InsertEventInNewEventsDatabase_DummyBedDocument_Success()
		{
			var bed = new DummyBed { Data = new List<int>() { 1, 2, 3 }, Condition = true };
			_eventDatabase.InsertEventInNewEventsDatabase(bed);

			Assert.That(_eventDatabase.GetNewEventsDocumentCount(), Is.EqualTo(1));
		}

		[Test]
		public void GetNewEventSubTypeDocument_ExtractDummyBedData_Success()
		{
			var dummy = new DummyBed { Condition = true };

			_eventDatabase.InsertEventInNewEventsDatabase(dummy);
			var doc = _eventDatabase.GetNewEventDocument();

			_eventDatabase.GetNewEventSubTypeDocument(typeof(DummyBed));

			Assert.That(doc, Is.TypeOf<DummyBed>());
			Assert.That(doc, Is.Not.TypeOf<CareBed>());
		}

		[Test]
		public void FindFallEvents_FindTwo_Succes()
		{
			var data = new List<int>() { 1, 2, 3 };
			var probability = 80;
			var fall1 = new DummyFall { Data = data, Condition = probability };
			var fall2 = new DummyFall { Data = data, Condition = probability };

			_eventDatabase.InsertEventInNewEventsDatabase(fall1);
			_eventDatabase.InsertEventInNewEventsDatabase(fall2);

			var foundEvents = _eventDatabase.FindNewTypeEvents(typeof(FallEvent));
			Assert.That(foundEvents.Count, Is.EqualTo(2));
		}

		[Test]
		public void GetNewEventDocument_InsertTwoDocs_ReturnTheFirst()
		{
			var data = new List<int>() { 1, 2, 3 };
			var fall = new DummyFall { Data = data, Condition = 80 };
			var bed = new DummyBed { Data = data, Condition = true };

			_eventDatabase.InsertEventInNewEventsDatabase(bed);
			Thread.Sleep(500);
			_eventDatabase.InsertEventInNewEventsDatabase(fall);

			var doc = _eventDatabase.GetNewEventDocument();
			Assert.That(doc, Is.TypeOf<DummyBed>());
		}

		[Test]
		public void PopNewEventDocument_InsertTwoDocs_OneLeft()
		{
			var data = new List<int>() { 1, 2, 3 };
			var probability = 80;
			var condition = true;
			var fall = new DummyFall { Data = data, Condition = probability }; ;
			var bed = new DummyBed { Data = data, Condition = condition };

			_eventDatabase.InsertEventInNewEventsDatabase(bed);
			_eventDatabase.InsertEventInNewEventsDatabase(fall);

			var doc = _eventDatabase.PopNewEventDocument();
			var docCount = _eventDatabase.GetNewEventsDocumentCount();

			Assert.That(docCount, Is.EqualTo(1));
		}

		[Test]
		public void PopNewEventDocument_InsertTwoDocsAndTakeTwoOut_NoneLeft()
		{
			var data = new List<int>() { 1, 2, 3 };
			var probability = 80;
			var condition = true;
			var fall = new DummyFall { Data = data, Condition = probability }; ;
			var bed = new DummyBed { Data = data, Condition = condition }; ;

			_eventDatabase.InsertEventInNewEventsDatabase(bed);
			_eventDatabase.InsertEventInNewEventsDatabase(fall);

			var doc1 = _eventDatabase.PopNewEventDocument();
			var doc2 = _eventDatabase.PopNewEventDocument();
			var docCount = _eventDatabase.GetNewEventsDocumentCount();

			Assert.That(docCount, Is.EqualTo(0));
		}

		[Test]
		public void PopNewEventDocument_InsertOneDocAndTakeTwoOut_NullReturned()
		{
			var data = new List<int>() { 1, 2, 3 };
			var probability = 80;
			var fall = new DummyFall { Data = data, Condition = probability };

			_eventDatabase.InsertEventInNewEventsDatabase(fall);

			var doc1 = _eventDatabase.PopNewEventDocument();
			var doc2 = _eventDatabase.PopNewEventDocument();

			Assert.That(doc2, Is.EqualTo(null));
		}

		[Test]
		public void GetAllLatestDocuments_OneDocumentFound_OneDocumentReturned()
		{
			var data = new List<int>() { 1, 2, 3 };
			var condition = false;
			var bed = new DummyBed() { Condition = false, Data = data };

			_eventDatabase.InsertEventInNewEventsDatabase(bed);
			var doc = _eventDatabase.GetNewEventDocument();

			_eventDatabase.InsertEventInLatestEventsDatabase(doc); ;

			var documents = _eventDatabase.GetAllLatestDocuments();
			Assert.That(documents.Result.Count, Is.EqualTo(1));
		}

		[Test]
		public void GetAllLatestSubTypeDocuments_OneDocumentFound_OneDocumentReturned()
		{
			var data = new List<int>() { 1, 2, 3 };
			var condition = false;
			var dummy1 = new DummyBed() { Condition = false, Data = data };
			var dummy2 = new DummyBed() { Condition = true, Data = data };

			_eventDatabase.InsertEventInLatestEventsDatabase(dummy1);
			_eventDatabase.InsertEventInLatestEventsDatabase(dummy2);

			Thread.Sleep(500);

			var documents = _eventDatabase.GetAllLatestSubTypeDocuments(typeof(BedEvent));
			Assert.That(documents.Result.Count, Is.EqualTo(2));
		}

		[Test]
		public void PublishConclusion_NoDocumentsFound_NewDocumentInserted()
		{
			var data = new List<int>() { 1, 2, 3 };
			var bed = new DummyBed { Data = data, Condition = false };

			_eventDatabase.InsertEventInNewEventsDatabase(bed);
			var doc = _eventDatabase.GetNewEventDocument();
			doc.Condition = 100;

			_eventDatabase.PublishConclusion(doc);

			var documents = _eventDatabase.GetAllLatestDocuments();
			Assert.That(documents.Result.Count, Is.EqualTo(1));
		}

		[Test]
		public void PublishConclusion_SimilarDocumentFound_ReplaceDocument()
		{
			var data1 = new List<int>() { 1, 2, 3 };
			var data2 = new List<int>() { 4, 5, 6 };
			var bed1 = new DummyBed()
			{
				Data = data1,
				Condition = true
			};
			var bed2 = new DummyBed()
			{
				Data = data2,
				Condition = false
			};

			//Create new event
			_eventDatabase.InsertEventInNewEventsDatabase(bed1);
			var doc1 = _eventDatabase.GetNewEventDocument();

			//Insert into Latest
			_eventDatabase.InsertEventInLatestEventsDatabase(doc1);
			_eventDatabase.EmptyNewEventsDatabase();

			//Create second event of same type
			_eventDatabase.InsertEventInNewEventsDatabase(bed2);
			var doc2 = _eventDatabase.GetNewEventDocument();
			doc2.Condition = 0;

			//Publish second BaseEvent
			_eventDatabase.PublishConclusion(doc2);

			var documents = _eventDatabase.GetAllLatestDocuments();
			Assert.That(documents.Result.Count, Is.EqualTo(1));

			var secondBed = (int)documents.Result[0].Condition;
			Assert.That(secondBed, Is.EqualTo(0));

			var data = documents.Result[0].Data;
			Assert.That(data.Count, Is.EqualTo(3));
		}
	}
}