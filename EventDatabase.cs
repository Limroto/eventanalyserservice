using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using caalhp.Core.Events;
using EventTypes;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;

namespace EventAnalyserService
{
	public enum EventDataBases { NewEventCollection, LatestEventCollection, PublishedEventCollection };

	public class EventDatabase : IEventDatabase
	{
		private readonly MongoClient _dbClient;
		private readonly IMongoDatabase _database;

		private readonly string _databaseName = "EventDatabase";

		//Databases
		public readonly string DatabaseCollectionNew = "NewEventCollection";
		public readonly string DatabaseCollectionLatest = "LatestEventCollection";
		public readonly string DatabaseCollectionPublished = "PublishedEventCollection";

		//Element names inserted into the collection
		public readonly string TimeStamp = "Timestamp";
		public readonly string BasicOrAdvanced = "BasicOrAdvanced";
		public readonly string EventType = "EventType";
		public readonly string SubType = "EventSubType";
		public readonly string Data = "Data";
		public readonly string EventCondition = "EventCondition";

		public EventDatabase()
		{
			try
			{
				_dbClient = new MongoClient();
				_database = _dbClient.GetDatabase(_databaseName);
				RegisterHelper.CheckBaseLvl();
			}
			catch (Exception e)
			{
				Console.WriteLine("Something went wrong with the MongoDB - is it started?");
				Console.WriteLine(e.Message);
			}

			//EmptyNewEventsDatabase();
		}

		/// <summary>
		/// Empty all the data from specified collection in database
		/// </summary>
		public void EmptyNewEventsDatabase()
		{
			try
			{
				var collection = _database.GetCollection<dynamic>(DatabaseCollectionNew);
				collection.DeleteManyAsync(new BsonDocument()).Wait();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				throw;
			}
		}

		public void EmptyLatestsEventsDatabase()
		{
			try
			{
				var collection = _database.GetCollection<dynamic>(DatabaseCollectionLatest);
				collection.DeleteManyAsync(new BsonDocument()).Wait();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				throw;
			}
		}

		public void EmptyPublishedEventsDatabase()
		{
			try
			{
				var collection = _database.GetCollection<dynamic>(DatabaseCollectionPublished);
				collection.DeleteManyAsync(new BsonDocument()).Wait();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				throw;
			}
		}

		/// <summary>
		/// Insert an BaseEvent into the database. Timestamp is added automatically.
		/// </summary>
		/// <param name="inputBaseEvent">BaseEvent to insert.</param>
		public async void InsertEventInNewEventsDatabase(BaseEvent inputBaseEvent)
		{
			try
			{
				inputBaseEvent.Condition = (inputBaseEvent.Condition is bool ? (inputBaseEvent.Condition == true ? 100 : 0) : inputBaseEvent.Condition);

				var collection = _database.GetCollection<dynamic>(DatabaseCollectionNew);
				await collection.InsertOneAsync(inputBaseEvent);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}

		/// <summary>
		/// Find and pop the next document
		/// </summary>
		/// <returns>First found doc from the collection.</returns>
		public BaseEvent PopNewEventDocument()
		{
			try
			{
				var name = _database.GetCollection<BaseEvent>(DatabaseCollectionNew)
					.FindOneAndDeleteAsync(x => x.Id != Guid.Empty);

				return name.Result;
			}
			catch (Exception e)
			{
				Console.WriteLine("Could not pop element.");
				Console.WriteLine(e.Message);
				return null;
			}
		}

		/// <summary>
		/// Find a specific event
		/// </summary>
		/// <param name="collectionName">Name of collection to use.</param>
		/// <returns>First found doc from the collection.</returns>
		public BaseEvent GetNewEventDocument()
		{
			try
			{
				var name = _database.GetCollection<BaseEvent>(DatabaseCollectionNew)
				.Find(x => x.Id != Guid.Empty)
				.SortByDescending(x => x.TimeStamp)
				.FirstAsync();

				return name.Result;
			}
			catch (Exception)
			{
				return null;
			}
		}

		/// <summary>
		/// Find a specific event
		/// </summary>
		/// <param name="filterType">Type of event.</param>
		/// <returns>List of all found docs with given filter type.</returns>
		public List<BaseEvent> GetNewEventSubTypeDocument(Type filterType)
		{
			var collection = _database.GetCollection<BaseEvent>(DatabaseCollectionNew);

			var filter = Builders<BaseEvent>.Filter.Eq(SubType, filterType.ToString());
			var docs = collection.Find(filter).ToListAsync();
			docs.Wait();

			return docs.Result;
		}

		/// <summary>
		/// Finds a specific type of events for which there can be many implementations
		/// </summary>
		/// <param name="typeFilter">Event type to find</param>
		/// <returns>List of all found events matching the filter</returns>
		public List<BaseEvent> FindNewTypeEvents(Type typeFilter)
		{
			var name = _database.GetCollection<BaseEvent>(DatabaseCollectionNew)
				.Find(x => x.Id != Guid.Empty)
				.ToListAsync();

			return name.Result;
		}

		/// <summary>
		/// Returns all documents from the 'Latest' database
		/// </summary>
		/// <returns>List of all documents</returns>
		public async Task<List<BaseEvent>> GetAllLatestDocuments()
		{
			var filter = Builders<BaseEvent>.Filter.Eq("_t", "BaseEvent");

			//Sure thoses are the correct variables?
			try
			{
				var entities = await _database.GetCollection<BaseEvent>(DatabaseCollectionLatest)
				.Find(filter)
				.ToListAsync();

				return entities.ToList();
			}
			catch (Exception)
			{
				return new List<BaseEvent>();
			}
		}

		public async Task<List<BaseEvent>> GetAllLatestSubTypeDocuments(Type lookupType)
		{
			var typeName = lookupType.UnderlyingSystemType.Name;
			var filter = Builders<BaseEvent>.Filter.Eq("_t", typeName);

			//Sure thoses are the correct variables?
			try
			{
				var entities = await _database.GetCollection<BaseEvent>(DatabaseCollectionLatest)
				.Find(filter)
				.ToListAsync();

				return entities.ToList();
			}
			catch (Exception e)
			{
				Console.WriteLine("Could not get all Lastest sub-types");
				Console.WriteLine(e.Message);
				return new List<BaseEvent>();
			}
		}

		/// <summary>
		/// Returns the amount of documents in the collection for the EventDatabsae
		/// </summary>
		/// <param name="collectionName">Name of collection to look into.</param>
		/// <returns>Amount count in collection.</returns>
		public int GetNewEventsDocumentCount()
		{
			try
			{
				var collection = _database.GetCollection<dynamic>(DatabaseCollectionNew);
				var docs = collection.CountAsync(new BsonDocument());
				docs.Wait();
				return (int)docs.Result;
			}
			catch (Exception e)
			{
				Console.WriteLine("GetNewEventsDocumentCount(): " + e.Message);
				throw;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="newEvent"></param>
		/// <param name="likelyhoodOfEvent"></param>
		public void PublishConclusion(BaseEvent newEvent)
		{
			try
			{
				//Store device unique ID in database, read this and base decision on this
				var eventUnit = newEvent.GetType().Name;

				var filter = Builders<BaseEvent>.Filter.Eq("_t", eventUnit);

				//var entities = _database.GetCollection<BaseEvent>(DatabaseCollectionLatest)
				//	.Find(filter)
				//	.SortByDescending(c => c.TimeStamp)
				//	.ToListAsync().Result;

				//Delete old event from Latest
				var tmp = _database.GetCollection<BaseEvent>(DatabaseCollectionLatest).DeleteOneAsync(filter).Result;

				InsertEventInLatestEventsDatabase(newEvent);
				InsertEventInPublishedEventsDatabase(newEvent);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}

		public async void InsertEventInLatestEventsDatabase(BaseEvent inputBaseEvent)
		{
			try
			{
				var collection = _database.GetCollection<BaseEvent>(DatabaseCollectionLatest);
				await collection.InsertOneAsync(inputBaseEvent);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}

		public async void InsertEventInPublishedEventsDatabase(BaseEvent inputBaseEvent)
		{
			try
			{
				var collection = _database.GetCollection<BaseEvent>(DatabaseCollectionPublished);
				await collection.InsertOneAsync(inputBaseEvent);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}
	}
}