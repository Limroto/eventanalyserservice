﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventTypes;

namespace EventAnalyserService
{
	public class InterpreterContextEvent : IInterpreterContextEvent
	{
		public List<BaseEvent> Events { get; private set; }
		public bool ShouldAnalyseEvents { get; set; }
		public double PlausibleFactor { get; private set; }
		private readonly IEventDatabase _eventDatabase;

		public InterpreterContextEvent(IEventDatabase eventDatabase)
		{
			ShouldAnalyseEvents = true;
			Events = new List<BaseEvent>();
			_eventDatabase = eventDatabase;
			PlausibleFactor = 0.5;
		}

		public void Add(BaseEvent evt)
		{
			Events.Add(evt);
		}

		/// <summary>
		/// Will analyse events as long as ShouldAnalyseEvents is true
		/// </summary>
		public void AnalyseEvents()
		{
			var newEvent = _eventDatabase.PopNewEventDocument();
			while (newEvent != null)
			{
				//var takeAction = ShouldTakeAction(newEvent);

				//if (takeAction)
				//{
				//<What event, time span to look back>
				var affectingEvents = EventsAffectingIt(newEvent);
				foreach (var affectingEvent in affectingEvents)
				{
					var eventFound = ReadPreviousEventDB(affectingEvent.Key, affectingEvent.Value);

					newEvent.Condition = ProbabilityAffection(newEvent, eventFound);
				}
				//}

				_eventDatabase.PublishConclusion(newEvent);
				Console.WriteLine("{0} occurred with {1}% chance.", newEvent.GetType(), newEvent.Condition);

				newEvent = _eventDatabase.PopNewEventDocument();
			}
		}

		public int ProbabilityAffection(BaseEvent newEvent, IEnumerable<BaseEvent> eventsFound)
		{
			var probability = newEvent.Condition;

			foreach (var baseEvent in eventsFound)
			{
				var condition = (baseEvent.Condition is bool ? (baseEvent.Condition == true ? 100 : 0) : baseEvent.Condition);
				probability = probability * ((100 - (condition * PlausibleFactor)) / 100);
			}

			return (int)probability;
		}

		public List<BaseEvent> ReadPreviousEventDB(Type typeToLookfor, int timeToLookBack)
		{
			var res = new List<BaseEvent>();
			var latestsEvents = _eventDatabase.GetAllLatestSubTypeDocuments(typeToLookfor).Result;

			foreach (var latest in latestsEvents)
			{
				if (DateTime.Parse(latest.TimeStamp).AddSeconds(timeToLookBack) > DateTime.Now)
				{
					res.Add(latest);
				}
			}
			return res;
		}

		public Dictionary<Type, int> EventsAffectingIt(BaseEvent newEvent)
		{
			var affects = new Dictionary<Type, int>();

			try
			{
				if (RegisterHelper.Instance.AffectionTable.ContainsKey(newEvent.GetType())
					|| RegisterHelper.Instance.AffectionTable.ContainsKey(newEvent.GetType().BaseType))
				{
					var tmp =
						RegisterHelper.Instance.AffectionTable.Where(
							x => x.Key == newEvent.GetType() || x.Key == newEvent.GetType().BaseType);

					foreach (var innerPair in tmp.SelectMany(pair => pair.Value))
					{
						affects.Add(innerPair.Key, innerPair.Value);
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}

			return affects;
		}

		public void InsertEvent(BaseEvent baseEvent)
		{
			try
			{
				_eventDatabase.InsertEventInNewEventsDatabase(baseEvent);
			}
			catch (AggregateException e)
			{
				Console.WriteLine(e);
			}
		}

		public bool ShouldTakeAction(BaseEvent newEvent)
		{
			var element = newEvent.GetType().BaseType.BaseType.ToString();

			if (element == typeof(Basic).ToString())
				return false;
			return true;
		}
	}
}
