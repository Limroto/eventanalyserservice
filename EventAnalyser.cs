﻿using System;
using System.Collections.Generic;
using caalhp.Core.Contracts;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using caalhp.Core.Utils.Helpers.Serialization.JSON;
using EventTypes;
using EventTypes.TestEvents;

namespace EventAnalyserService
{
	public class EventAnalyser : IServiceCAALHPContract, IEventAnalyser
	{
		public IServiceHostCAALHPContract Host { get; set; }
		private readonly IInterpreterContextEvent _interpreter;
		private readonly List<string> _subscriptionList;
		private int _processId;
		private Guid _latestGuid = Guid.Empty;

		public EventAnalyser(IInterpreterContextEvent interpreter)
		{
			_interpreter = interpreter;
			_subscriptionList = new List<string>();
		}

		public void Notify(KeyValuePair<string, string> notification)
		{
			try
			{
				var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
				if (type.BaseType != null && type.BaseType.BaseType == typeof(BaseEvent))
					return;

				dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
				if (obj == null)
					return;

				//Duplicated events comming in
				var newId = (obj as BaseEvent).Id;
				if (newId == _latestGuid)
					return;

				_latestGuid = newId;

				Host.Host.SubscribeToEvents(notification.Key, _processId);

				_subscriptionList.Add(notification.Key);
				_interpreter.InsertEvent(obj);
				_interpreter.AnalyseEvents();
			}
			catch (Exception e)
			{
				Console.WriteLine("Error during Notify " + e);
			}
		}

		public string GetName()
		{
			return "EventAnalyserService";
		}

		public bool IsAlive()
		{
			return true;
		}

		public void ShutDown()
		{
			Environment.Exit(0);
		}

		public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
		{
			Host = hostObj;
			_processId = processId;

			SubscribeToAllEvents();

			Console.WriteLine("EventAnalyserService Initialized");
		}

		private void SubscribeToAllEvents()
		{
			ListenToEvents(new DummyFall());
			ListenToEvents(new DummyBed());
			ListenToEvents(new Shimmer());
			ListenToEvents(new CareBed());

			//RegisterHelper.Instance.AddAffection(typeof(DummyFall), typeof(BedEvent), 10);
			RegisterHelper.Instance.AddAffection(typeof(FallEvent), typeof(BedEvent), 10);

			Console.WriteLine("Subscribed to BaseEvent");
		}

		public void Start()
		{
		}

		public void Stop()
		{
			Environment.Exit(0);
		}

		/// <summary>
		/// Events to subscripe to. Events must be "level 1" objects.
		/// </summary>
		/// <param name="type">A level 1 event - This is the concrete implementation.</param>
		/// <param name="assemblyName">Name of assambly event is stored in.</param>
		private void ListenToEvents(BaseEvent type, string assemblyName = "EventTypes")
		{
			var genericInfo = EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json,
				type.GetType(), assemblyName);
			Host.Host.SubscribeToEvents(genericInfo, _processId);

			RegisterHelper.RegisterNewClass(type);
		}
	}
}
